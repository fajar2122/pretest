<script>
    //no 1
    const array = [
        {
            name: 'John', age: 21
        },
        {
            name: 'Daniel', 
            age: 30
        }
    ];
    const array2 = [
        {
            name: 'Josh', age: 28
        }, 
        {
            name: 'Luke', age: 32
        }
    ];

    // no 2
    const merge = [...array, ...array2];
    console.log(merge);

    const index = array.findIndex(obj => {
        return obj.name === 'John';
    });
    console.log(index);

    array[index].age = 22;
    console.log(array);
    
    // no 3
    const popped_object = array.pop();
    console.log(popped_object);

    // no 4
    
    // const array = [
    //     {
    //         name: 'John', age: 21
    //     },
    //     {
    //         name: 'Daniel', age: 30
    //     }
    // ];
    // array.splice( 'VollyBall');
    // console.log(array);

    // array.splice(4, 1, 'BasketBall');
    // console.log(array);

    // no 5
    const array5 = [
    {name: 'John', age: 21}, {name: 'Daniel', age:30},
    {name: 'Josh', age: 28}, {name: 'Luke', age: 32}
];
console.log(array5.find(a => a.age > 30));



</script>